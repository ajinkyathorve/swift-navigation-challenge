#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 21 23:36:47 2017

@author: aj
"""

import math

def fibonacci(n):
       '''
       Function to calculate the first n Fibonacci numbers
       '''
       f0 = 0
       f1 = 1
       counter = 0
       while (counter < n):
              yield f0
              f0, f1 = f1, f0 + f1
              counter += 1


def check_prime(num):
       '''
       Function to check if a given number is prime
       '''
       # Since 0 and 1 are not primes, return False if we get those
       if num < 2:
              return False
       # Check if the number is divisible by any other number from 2 upto the square root of that number
       for i in range(2,int(math.ceil(math.sqrt(num)))):
           if num % i == 0:
               return False
               i += 1
       # Return True if all tests failed, it means the number is prime
       return True


def main():
       # Accept value of n from the user
       n = int(raw_input('Please enter how many Fibonacci numbers you want to generate: ').strip())
       
       # Call function to calculate Fibonacci sequence
       fib_sequence = fibonacci(n)
       
       '''
       Print the output in the required format, which is:
       "Buzz" if number is divisible by 3.
       "Fizz" if number is divisible by 5.
       "FizzBuzz" if number is divisible by 15.
       "BuzzFizz"  if number is prime.
       The number otherwise.
       '''
       print '\nThe required output is:'
       for num in fib_sequence:
              if (num % 3 == 0 and num % 5 == 0):
                     print 'FizzBuzz'
                     continue
              elif num % 3 == 0:
                      print 'Buzz'
                      continue
              elif num % 5 == 0:
                      print 'Fizz'
                      continue
              elif check_prime(num):
                     print 'BuzzFizz'
                     continue
              else:
                     print num


if __name__ == '__main__':
    main()
